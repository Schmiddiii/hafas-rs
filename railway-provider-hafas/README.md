# Railway Hafas Provider

Implementation of a HAFAS client in Rust.

This crate is part of [railway-backend](https://gitlab.com/schmiddi-on-mobile/railway-backend).
You can find a high-level documentation of railway-backend [here](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tree/main/docs?ref_type=heads).

This was originally forked from [hafas-rs](https://cyberchaos.dev/yuka/hafas-rs/), but later heavily refactored to create a generic backend for Railway.
It takes a lot of inspiration from the JavaScript [hafas-client](https://github.com/public-transport/hafas-client) library, which is licensed under ISC.

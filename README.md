# Railway Backend

This set of crates provide the backend for [Railway](https://gitlab.com/schmiddi-on-mobile/railway).
This includes core struct and trait definitions, different provider implementations and a high-level API Railway uses.

You can find a high-level documentation of the crates in the [docs](./docs) folder.

## Profiles

The following profiles are currently supported:

| Short Name      | Long Name                                           |
|-----------------|-----------------------------------------------------|
| AVV             | Aachener Verkehrsverbund                            |
| BART            | Bay Area Rapid Transit                              |
| BLS             | BLS AG                                              |
| CFL             | Société Nationale des Chemins de Fer Luxembourgeois |
| CMTA            | Austin, Texas                                       |
| DART            | Des Moines Area Rapid Transit                       |
| DB              | Deutsche Bahn                                       |
| Irish-Rail      | Iarnród Éireann                                     |
| IVB             | Innsbrucker Verkehrsbetriebe                        |
| KVB             | Kölner Verkehrs-Betriebe                            |
| Mobiliteit-Lu   | Mobilitéitszentral                                  |
| NahSH           | Nah.SH                                              |
| NVV             | Nordhessischer Verkehrsverbund                      |
| OEBB            | Österreichische Bundesbahnen                        |
| OOEVV           | Oberösterreichischer Verkehrsverbund                |
| PKP             | Polskie Koleje Państwowe                            |
| Rejseplanen     | Rejseplanen in Denmark                              |
| Resrobot        | Samtrafiken Resrobot in Sweden                      |
| RMV             | Rhein-Main-Verkehrsverbund                          |
| RSAG            | Rostocker Straßenbahn AG                            |
| S-Bahn-Muenchen | S-Bahn München                                      |
| saarVV          | Saarfahrplan/VGS Saarland                           |
| Salzburg        | Salzburg                                            |
| search.ch       | search.ch for SBB (Switzerland)                     |
| SVV             | Salzburger Verkehrsverbund                          |
| Transitous      | [Transitous](https://transitous.org/)               |
| VBB             | Berlin &amp; Brandenburg public transport           |
| VBN             | Verkehrsverbund Bremen/Niedersachsen                |
| Verbundlinie    | Verbundlinie                                        |
| VGI             | Ingolstädter Verkehrsgesellschaft                   |
| VKG             | Kärntner Linien/Verkehrsverbund Kärnten             |
| VMT             | Verkehrsverbund Mittelthüringen                     |
| VSN             | Verkehrsverbund Süd-Niedersachsen                   |
| VOR             | Verkehrsverbund Ost-Region                          |
| VOS             | Verkehrsgemeinschaft Osnabrück                      |
| VRN             | Verkehrsverbund Rhein-Neckar                        |
| VVT             | Verkehrsverbund Tirol                               |
| VVV             | Verkehrsverbund Vorarlberg                          |

The following profiles are currently being worked on:

| Short Name      | Long Name                                           | Reason                                                                                                  |
|-----------------|-----------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| BVG             | Berlin public transport                             | Too many customizations for now                                                                         |
| DB-Busradar-NRW | DB Busradar NRW                                     | Always shows "No Connection"                                                                            |
| INSA            | Nahverkehr Sachsen-Anhalt                           | Always shows "Location Missing or Invalid"                                                              |
| ZVV             | Zürich public transport                             | Authorization Failure                                                                                   |
| TPG             | Transports publics genevois                         | Fails with "time outside of timetable period"                                                           |
| mobil-nrw       | mobil.nrw                                           | Migrated endpoint to EFA (<https://github.com/alexander-albers/tripkit/commit/535ab49a6d174076fd0733d398f9939f815d5363>) |

The following will probably never work:

| Short Name      | Long Name                                           | Reason                                                                                                  |
|-----------------|-----------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| HVV             | Hamburger Verkehrsverbund                           | Shut off endpoint <https://github.com/public-transport/hafas-client/issues/271#issuecomment-1262363078> |
| SBB             | Schweizerische Bundesbahnen                         | Shut off endpoint <https://github.com/public-transport/hafas-client/issues/271#issuecomment-1262363078> |
| SNCB            | Belgian National Railways                           | Shut off endpoint <https://github.com/public-transport/hafas-client/issues/284>                         |
| SNCF            | Société nationale des chemins de fer français       | Shut off endpoint                                                                                       |

Note that some are gated behind non-default feature flags but broken.

## Feature Flags

Feature flags of the high-level API:

| Name               | Description                                                 | Note                                                                                       |
|--------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------|
| *-provider         | Enable a certain provider                                   | Not every provider gated behind its feature gate may work. Consult above table for details | 
| all-profiles       | Enable all default profiles which should work               |                                                                                            |
| rt-multi-thread    | Allow moving the client and hyper-requester between threads |                                                                                            |
| polylines          | Allow returning polylines from journey results              | Untested                                                                                   |

Feature flags of the core:

| Name               | Description                                                 | Note                                                                                       |
|--------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------|
| reqwest-requester  | Enable requesting data with reqwest                         |                                                                                            |
| hyper-requester    | Enable requesting data with hyper                           | Deprecated in favor of reqwest.                                                            |

## Code of Conduct

This project follows [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).

## License

railway-backend itself is dual-licensed AGPL-3.0-or-later and EUPL-1.2.

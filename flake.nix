{
  # keep in sync with metainfo
  description = "Backend implementation of Railway";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
        in
        { 
          devShells.default =
            with pkgs;
            pkgs.mkShell {
              src = ./.;
              buildInputs = [];
              nativeBuildInputs = [ gcc pkg-config openssl clippy rustc ];
            };
        })
    );
}

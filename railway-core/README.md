# Railway Core

Core type definitions of the Railway backend.

This crate is part of [railway-backend](https://gitlab.com/schmiddi-on-mobile/railway-backend).
You can find a high-level documentation of railway-backend [here](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tree/main/docs?ref_type=heads).

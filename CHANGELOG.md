# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased railway-provider-motis

### Changed

- Rewrite to new Motis 2 API.

## Unreleased railway-provider-db-movas

### Fixed

- Error on stops with no platform.

## [railway-core 0.1.1] - 2025-01-19

### Added

- Reqwest requester.

### Deprecated

- Hyper requester.

## [railway-api 0.1.1] - 2025-01-19

### Changed

- Replaced old DB Hafas provider with new Movas provider.

## [railway-api-derive 0.1.1] - 2025-01-19

### Fixed

- Clearer compile error when no provider is activated.

## [railway-provider-db-movas 0.1.0] - 2025-01-19

### Added

- New provider to replace the removed DB Hafas provider.

## [railway-provider-hafas 0.1.2] - 2025-01-19

### Added

- Feature to activate all supported profiles.
- Resrobot (https://reseplanerare.resrobot.se/) profile.

### Removed

- Profiles which did not compile correctly.
- DB provider; it was shut off and replaced by the movas provider.

### Fixed

- Instead of throwing an error when the timezone is ambiguous, use the earlier.
- Fix VGI profile returning invalid load factor entries.

## [railway-provider-hafas 0.1.1] - 2024-06-12

### Changed

- Remove `lib.crate-type` from `Cargo.toml`.

### Fixed

- Walks with 0 minutes between most legs.

## [railway-core 0.1.0] - 2024-06-12

Initial release.

## [railway-provider-hafas 0.1.0] - 2024-06-12

Initial release.

## [railway-provider-motis 0.1.0] - 2024-06-12

Initial release.

## [railway-provider-search-ch 0.1.0] - 2024-06-12

Initial release.

## [railway-api-derive 0.1.0] - 2024-06-12

Initial release.

## [railway-api 0.1.0] - 2024-06-12

Initial release.

# Changelog hafas-rs 

Before the refactor to `railway-backend`, this repository was called `hafas-rs`.
This section lists the previous changelog.

## [0.2.3] - 2024-04-30

### Fixed

- RMV invalid load factor

## [0.2.2] - 2024-03-29

### Added

- Added back PKP profile.

### Fixed

- Mobiliteit.lu errors.

### Removed

- INSA due to throwing errors.

## [0.2.1] - 2024-03-03

### Fixed

- Rejseplanen profile leading to errors with buses.

## [0.2.0] - 2024-01-09

### Added

- Frequency Information to Legs

### Changed

- Coarser `PartialEq` implementation for `Place`.

### Chores

- Clippy fixes.
- Dependency Updates 

## [0.1.0] - 2023-09-24

- Initial Release

[railway-provider-api 0.1.1]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/railway-api-0.1.0...railway-api-0.1.1
[railway-provider-api-derive 0.1.1]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/railway-api-derive-0.1.0...railway-api-derive-0.1.1
[railway-provider-db-movas 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-provider-db-movas-0.1.0
[railway-provider-hafas 0.1.2]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/railway-provider-hafas-0.1.1...railway-provider-hafas-0.1.2
[railway-provider-hafas 0.1.1]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/railway-provider-hafas-0.1.0...railway-provider-hafas-0.1.1
[railway-core 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-core-0.1.0
[railway-provider-hafas 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-provider-hafas-0.1.0
[railway-provider-motis 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-provider-motis-0.1.0
[railway-provider-search-ch 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-provider-search-ch-0.1.0
[railway-api-derive 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-api-derive-0.1.0
[railway-api 0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/railway-api-0.1.0
[0.2.3]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/0.2.3...0.2.0
[0.2.2]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/0.2.2...0.2.1
[0.2.1]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/0.2.1...0.2.0
[0.2.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/compare/0.2.0...0.1.0
[0.1.0]: https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tags/0.1.0

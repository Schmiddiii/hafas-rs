# Contributing

Contributions to railway-backend are always welcome. There are many different ways you can contribute, this document will show you most of these ways and what you should keep in mind.

We are interested in getting to know our contributors and are often around to discuss the state and the future of Railway and its backend, be it something minor, or larger visions. Feel free to join our [Matrix channel](https://matrix.to/#/#railwayapp:matrix.org) and talk with us! This is also a good place to ask some questions or report smaller issues.

Note that the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) applies to this project, therefore, be nice to each other.

## Open Issues

Issues are a good way to tell me problems you are having with the applications or things that you feel are missing or might be improved. There are a few things to keep in mind with issues.

- Add the logs if you think they are relevant, this is mostly useful for errors that occurred.
- Check for duplicated issues: Try to use the search-feature if you can find similar issues like you are having. If there is already such an issue, consider giving it a thumbs-up or commenting more details on that issue, but don't create a new issue.
- Also check the [Railway issue list](https://gitlab.com/schmiddi-on-mobile/railway/-/issues) if something similar was reported there.
- Know how to write a good issue: Read e.g. <https://wiredcraft.com/blog/how-we-write-our-github-issues/> (also applies pretty much got GitLab)

## Write Code

If you feel comfortable enough writing code, you can also submit your changes directly via a merge request.
Please look at the [developer documentation](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tree/main/docs?ref_type=heads) for further information.

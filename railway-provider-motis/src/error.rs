use std::fmt::Display;

#[derive(Debug)]
pub enum Error {
    Geocoding(motis::apis::Error<motis::apis::geocode_api::GeocodeError>),
    Plan(motis::apis::Error<motis::apis::routing_api::PlanError>),
    ChronoParsing(chrono::format::ParseError),
    RefreshJourneyNotFound,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match &self {
            Self::Geocoding(e) => writeln!(f, "failure to geocode: {}", e),
            Self::Plan(e) => writeln!(f, "failure to plan trip: {}", e),
            Self::ChronoParsing(e) => writeln!(f, "failed to parse date-time returned: {}", e),
            Self::RefreshJourneyNotFound => write!(f, "refresh journey not found"),
        }
    }
}

impl std::error::Error for Error {}

/*
 * MOTIS API
 *
 * This is the MOTIS routing API.
 *
 * The version of the OpenAPI document: v1
 * Contact: felix@triptix.tech
 * Generated by: https://openapi-generator.tech
 */

use crate::models;
use serde::{Deserialize, Serialize};

/// Duration : Object containing duration if a path was found or none if no path was found
#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Duration {
    /// duration in seconds if a path was found, otherwise missing
    #[serde(rename = "duration", skip_serializing_if = "Option::is_none")]
    pub duration: Option<f64>,
}

impl Duration {
    /// Object containing duration if a path was found or none if no path was found
    pub fn new() -> Duration {
        Duration {
            duration: None,
        }
    }
}


# PedestrianProfile

## Enum Variants

| Name | Value |
|---- | -----|
| Foot | FOOT |
| Wheelchair | WHEELCHAIR |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



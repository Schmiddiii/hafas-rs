# Mode

## Enum Variants

| Name | Value |
|---- | -----|
| Walk | WALK |
| Bike | BIKE |
| Rental | RENTAL |
| Car | CAR |
| CarParking | CAR_PARKING |
| Transit | TRANSIT |
| Tram | TRAM |
| Subway | SUBWAY |
| Ferry | FERRY |
| Airplane | AIRPLANE |
| Metro | METRO |
| Bus | BUS |
| Coach | COACH |
| Rail | RAIL |
| HighspeedRail | HIGHSPEED_RAIL |
| LongDistance | LONG_DISTANCE |
| NightRail | NIGHT_RAIL |
| RegionalFastRail | REGIONAL_FAST_RAIL |
| RegionalRail | REGIONAL_RAIL |
| Other | OTHER |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



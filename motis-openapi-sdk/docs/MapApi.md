# \MapApi

All URIs are relative to *https://europe.motis-project.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**initial**](MapApi.md#initial) | **GET** /api/v1/map/initial | initial location to view the map at after loading based on where public transport should be visible
[**levels**](MapApi.md#levels) | **GET** /api/v1/map/levels | Get all available levels for a map section
[**stops**](MapApi.md#stops) | **GET** /api/v1/map/stops | Get all stops for a map section
[**trips**](MapApi.md#trips) | **GET** /api/v1/map/trips | Given a area frame (box defined by top right and bottom left corner) and a time frame, it returns all trips and their respective shapes that operate in this area + time frame. Trips are filtered by zoom level. On low zoom levels, only long distance trains will be shown while on high zoom levels, also metros, buses and trams will be returned. 



## initial

> models::Initial200Response initial()
initial location to view the map at after loading based on where public transport should be visible

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Initial200Response**](initial_200_response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## levels

> Vec<f64> levels(min, max)
Get all available levels for a map section

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**min** | **String** | latitude,longitude pair of the lower right coordinate | [required] |
**max** | **String** | latitude,longitude pair of the upper left coordinate | [required] |

### Return type

**Vec<f64>**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## stops

> Vec<models::Place> stops(min, max)
Get all stops for a map section

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**min** | **String** | latitude,longitude pair of the lower right coordinate | [required] |
**max** | **String** | latitude,longitude pair of the upper left coordinate | [required] |

### Return type

[**Vec<models::Place>**](Place.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## trips

> Vec<models::TripSegment> trips(zoom, min, max, start_time, end_time)
Given a area frame (box defined by top right and bottom left corner) and a time frame, it returns all trips and their respective shapes that operate in this area + time frame. Trips are filtered by zoom level. On low zoom levels, only long distance trains will be shown while on high zoom levels, also metros, buses and trams will be returned. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**zoom** | **f64** | current zoom level | [required] |
**min** | **String** | latitude,longitude pair of the lower right coordinate | [required] |
**max** | **String** | latitude,longitude pair of the upper left coordinate | [required] |
**start_time** | **String** | start of the time window | [required] |
**end_time** | **String** | end if the time window | [required] |

### Return type

[**Vec<models::TripSegment>**](TripSegment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


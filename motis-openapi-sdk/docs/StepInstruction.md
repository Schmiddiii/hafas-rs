# StepInstruction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relative_direction** | [**models::Direction**](Direction.md) |  | 
**distance** | **f64** | The distance in meters that this step takes. | 
**from_level** | **f64** | level where this segment starts, based on OpenStreetMap data | 
**to_level** | **f64** | level where this segment starts, based on OpenStreetMap data | 
**osm_way** | Option<**i32**> | OpenStreetMap way index | [optional]
**polyline** | [**models::EncodedPolyline**](EncodedPolyline.md) |  | 
**street_name** | **String** | The name of the street. | 
**exit** | **String** | Not implemented! When exiting a highway or traffic circle, the exit name/number.  | 
**stay_on** | **bool** | Not implemented! Indicates whether or not a street changes direction at an intersection.  | 
**area** | **bool** | Not implemented! This step is on an open area, such as a plaza or train platform, and thus the directions should say something like \"cross\"  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Duration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | Option<**f64**> | duration in seconds if a path was found, otherwise missing | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



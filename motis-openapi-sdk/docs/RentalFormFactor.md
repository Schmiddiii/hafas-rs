# RentalFormFactor

## Enum Variants

| Name | Value |
|---- | -----|
| Bicycle | BICYCLE |
| CargoBicycle | CARGO_BICYCLE |
| Car | CAR |
| Moped | MOPED |
| ScooterStanding | SCOOTER_STANDING |
| ScooterSeated | SCOOTER_SEATED |
| Other | OTHER |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



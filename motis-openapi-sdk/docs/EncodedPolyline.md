# EncodedPolyline

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**points** | **String** | The encoded points of the polyline using the Google polyline encoding with precision 7. | 
**length** | **i32** | The number of points in the string | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



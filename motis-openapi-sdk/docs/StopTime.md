# StopTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**place** | [**models::Place**](Place.md) | information about the stop place and time | 
**mode** | [**models::Mode**](Mode.md) | Transport mode for this leg | 
**real_time** | **bool** | Whether there is real-time data about this leg | 
**headsign** | **String** | For transit legs, the headsign of the bus or train being used. For non-transit legs, null  | 
**agency_id** | **String** |  | 
**agency_name** | **String** |  | 
**agency_url** | **String** |  | 
**route_color** | Option<**String**> |  | [optional]
**route_text_color** | Option<**String**> |  | [optional]
**trip_id** | **String** |  | 
**route_short_name** | **String** |  | 
**source** | **String** | Filename and line number where this trip is from | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Footpaths200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**place** | [**models::Place**](Place.md) |  | 
**footpaths** | [**Vec<models::Footpath>**](Footpath.md) | all outgoing footpaths of this location | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



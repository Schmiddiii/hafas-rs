# TripInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trip_id** | **String** | trip ID (dataset trip id prefixed with the dataset tag) | 
**route_short_name** | **String** | trip display name | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



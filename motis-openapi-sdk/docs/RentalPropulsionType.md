# RentalPropulsionType

## Enum Variants

| Name | Value |
|---- | -----|
| Human | HUMAN |
| ElectricAssist | ELECTRIC_ASSIST |
| Electric | ELECTRIC |
| Combustion | COMBUSTION |
| CombustionDiesel | COMBUSTION_DIESEL |
| Hybrid | HYBRID |
| PlugInHybrid | PLUG_IN_HYBRID |
| HydrogenFuelCell | HYDROGEN_FUEL_CELL |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



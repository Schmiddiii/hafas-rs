# Area

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the area | 
**admin_level** | **f64** | [OpenStreetMap `admin_level`](https://wiki.openstreetmap.org/wiki/Key:admin_level) of the area  | 
**matched** | **bool** | Whether this area was matched by the input text | 
**default** | Option<**bool**> | Whether this area should be displayed as default area (area with admin level closest 7) | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



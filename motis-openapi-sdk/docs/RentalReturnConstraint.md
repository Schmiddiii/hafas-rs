# RentalReturnConstraint

## Enum Variants

| Name | Value |
|---- | -----|
| None | NONE |
| AnyStation | ANY_STATION |
| RoundtripStation | ROUNDTRIP_STATION |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \DebugApi

All URIs are relative to *https://europe.motis-project.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**footpaths**](DebugApi.md#footpaths) | **GET** /api/debug/footpaths | Prints all footpaths of a timetable location (track, bus stop, etc.)



## footpaths

> models::Footpaths200Response footpaths(id)
Prints all footpaths of a timetable location (track, bus stop, etc.)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | location id | [required] |

### Return type

[**models::Footpaths200Response**](footpaths_200_response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


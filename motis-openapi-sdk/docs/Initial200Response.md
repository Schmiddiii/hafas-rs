# Initial200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **f64** | latitude | 
**lon** | **f64** | longitude | 
**zoom** | **f64** | zoom level | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



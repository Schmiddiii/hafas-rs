# \GeocodeApi

All URIs are relative to *https://europe.motis-project.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**geocode**](GeocodeApi.md#geocode) | **GET** /api/v1/geocode | Autocompletion & geocoding that resolves user input addresses including coordinates
[**one_to_many**](GeocodeApi.md#one_to_many) | **GET** /api/v1/one-to-many | Street routing from one to many places or many to one. The order in the response array corresponds to the order of coordinates of the \\`many\\` parameter in the query. 
[**reverse_geocode**](GeocodeApi.md#reverse_geocode) | **GET** /api/v1/reverse-geocode | Translate coordinates to the closest address(es)/places/stops.



## geocode

> Vec<models::Match> geocode(text, language)
Autocompletion & geocoding that resolves user input addresses including coordinates

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**text** | **String** | the (potentially partially typed) address to resolve | [required] |
**language** | Option<**String**> | language tags as used in OpenStreetMap (usually ISO 639-1, or ISO 639-2 if there's no ISO 639-1)  |  |

### Return type

[**Vec<models::Match>**](Match.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## one_to_many

> Vec<models::Duration> one_to_many(one, many, mode, max, max_matching_distance, arrive_by)
Street routing from one to many places or many to one. The order in the response array corresponds to the order of coordinates of the \\`many\\` parameter in the query. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**one** | **String** | geo location as latitude,longitude | [required] |
**many** | [**Vec<String>**](String.md) | geo locations as latitude;longitude,latitude;longitude,... | [required] |
**mode** | [**Mode**](.md) | routing profile to use (currently supported: \\`WALK\\`, \\`BIKE\\`, \\`CAR\\`)  | [required] |
**max** | **f64** | maximum travel time in seconds | [required] |
**max_matching_distance** | **f64** | maximum matching distance in meters to match geo coordinates to the street network | [required] |
**arrive_by** | **bool** | true = many to one false = one to many  | [required] |

### Return type

[**Vec<models::Duration>**](Duration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## reverse_geocode

> Vec<models::Match> reverse_geocode(place)
Translate coordinates to the closest address(es)/places/stops.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**place** | **String** | latitude, longitude in degrees | [required] |

### Return type

[**Vec<models::Match>**](Match.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


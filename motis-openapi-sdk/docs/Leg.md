# Leg

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mode** | [**models::Mode**](Mode.md) | Transport mode for this leg | 
**from** | [**models::Place**](Place.md) |  | 
**to** | [**models::Place**](Place.md) |  | 
**duration** | **i32** | Leg duration in seconds  If leg is footpath:   The footpath duration is derived from the default footpath   duration using the query parameters `transferTimeFactor` and   `additionalTransferTime` as follows:   `leg.duration = defaultDuration * transferTimeFactor + additionalTransferTime.`   In case the defaultDuration is needed, it can be calculated by   `defaultDuration = (leg.duration - additionalTransferTime) / transferTimeFactor`.   Note that the default values are `transferTimeFactor = 1` and   `additionalTransferTime = 0` in case they are not explicitly   provided in the query.  | 
**start_time** | **String** | leg departure time | 
**end_time** | **String** | leg arrival time | 
**scheduled_start_time** | **String** | scheduled leg departure time | 
**scheduled_end_time** | **String** | scheduled leg arrival time | 
**real_time** | **bool** | Whether there is real-time data about this leg | 
**distance** | Option<**f64**> | For non-transit legs the distance traveled while traversing this leg in meters. | [optional]
**interline_with_previous_leg** | Option<**bool**> | For transit legs, if the rider should stay on the vehicle as it changes route names. | [optional]
**headsign** | Option<**String**> | For transit legs, the headsign of the bus or train being used. For non-transit legs, null  | [optional]
**route_color** | Option<**String**> |  | [optional]
**route_text_color** | Option<**String**> |  | [optional]
**route_type** | Option<**String**> |  | [optional]
**agency_name** | Option<**String**> |  | [optional]
**agency_url** | Option<**String**> |  | [optional]
**agency_id** | Option<**String**> |  | [optional]
**trip_id** | Option<**String**> |  | [optional]
**route_short_name** | Option<**String**> |  | [optional]
**source** | Option<**String**> | Filename and line number where this trip is from | [optional]
**intermediate_stops** | Option<[**Vec<models::Place>**](Place.md)> | For transit legs, intermediate stops between the Place where the leg originates and the Place where the leg ends. For non-transit legs, null.  | [optional]
**leg_geometry** | [**models::EncodedPolyline**](EncodedPolyline.md) |  | 
**steps** | Option<[**Vec<models::StepInstruction>**](StepInstruction.md)> | A series of turn by turn instructions used for walking, biking and driving.  | [optional]
**rental** | Option<[**models::Rental**](Rental.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



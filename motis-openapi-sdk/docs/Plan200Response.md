# Plan200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_parameters** | **std::collections::HashMap<String, String>** | the routing query | 
**debug_output** | **std::collections::HashMap<String, i32>** | debug statistics | 
**from** | [**models::Place**](Place.md) |  | 
**to** | [**models::Place**](Place.md) |  | 
**direct** | [**Vec<models::Itinerary>**](Itinerary.md) | Direct trips by `WALK`, `BIKE`, `CAR`, etc. without time-dependency. The starting time (`arriveBy=false`) / arrival time (`arriveBy=true`) is always the queried `time` parameter (set to \\\"now\\\" if not set). But all `direct` connections are meant to be independent of absolute times.  | 
**itineraries** | [**Vec<models::Itinerary>**](Itinerary.md) | list of itineraries | 
**previous_page_cursor** | **String** | Use the cursor to get the previous page of results. Insert the cursor into the request and post it to get the previous page. The previous page is a set of itineraries departing BEFORE the first itinerary in the result for a depart after search. When using the default sort order the previous set of itineraries is inserted before the current result.  | 
**next_page_cursor** | **String** | Use the cursor to get the next page of results. Insert the cursor into the request and post it to get the next page. The next page is a set of itineraries departing AFTER the last itinerary in this result.  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



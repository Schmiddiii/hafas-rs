# VertexType

## Enum Variants

| Name | Value |
|---- | -----|
| Normal | NORMAL |
| Bikeshare | BIKESHARE |
| Transit | TRANSIT |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TripSegment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trips** | [**Vec<models::TripInfo>**](TripInfo.md) |  | 
**route_color** | Option<**String**> |  | [optional]
**mode** | [**models::Mode**](Mode.md) | Transport mode for this leg | 
**distance** | **f64** | distance in meters | 
**from** | [**models::Place**](Place.md) |  | 
**to** | [**models::Place**](Place.md) |  | 
**departure** | **String** | departure time | 
**arrival** | **String** | arrival time | 
**scheduled_departure** | **String** | scheduled departure time | 
**scheduled_arrival** | **String** | scheduled arrival time | 
**real_time** | **bool** | Whether there is real-time data about this leg | 
**polyline** | **String** | Google polyline encoded coordinate sequence (with precision 7) where the trip travels on this segment. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



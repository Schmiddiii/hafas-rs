# AbsoluteDirection

## Enum Variants

| Name | Value |
|---- | -----|
| North | NORTH |
| Northeast | NORTHEAST |
| East | EAST |
| Southeast | SOUTHEAST |
| South | SOUTH |
| Southwest | SOUTHWEST |
| West | WEST |
| Northwest | NORTHWEST |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



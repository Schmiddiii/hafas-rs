# RelativeDirection

## Enum Variants

| Name | Value |
|---- | -----|
| Depart | DEPART |
| HardLeft | HARD_LEFT |
| Left | LEFT |
| SlightlyLeft | SLIGHTLY_LEFT |
| Continue | CONTINUE |
| SlightlyRight | SLIGHTLY_RIGHT |
| Right | RIGHT |
| HardRight | HARD_RIGHT |
| CircleClockwise | CIRCLE_CLOCKWISE |
| CircleCounterclockwise | CIRCLE_COUNTERCLOCKWISE |
| Elevator | ELEVATOR |
| UturnLeft | UTURN_LEFT |
| UturnRight | UTURN_RIGHT |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



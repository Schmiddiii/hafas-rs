# Footpath

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | [**models::Place**](Place.md) |  | 
**default** | Option<**f64**> | optional; missing if the GTFS did not contain a footpath footpath duration in minutes according to GTFS (+heuristics)  | [optional]
**foot** | Option<**f64**> | optional; missing if no path was found (timetable / osr) footpath duration in minutes for the foot profile  | [optional]
**foot_routed** | Option<**f64**> | optional; missing if no path was found with foot routing footpath duration in minutes for the foot profile  | [optional]
**wheelchair** | Option<**f64**> | optional; missing if no path was found with the wheelchair profile  footpath duration in minutes for the wheelchair profile  | [optional]
**wheelchair_uses_elevator** | Option<**bool**> | optional; missing if no path was found with the wheelchair profile true if the wheelchair path uses an elevator  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



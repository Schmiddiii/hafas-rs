# LevelEncodedPolyline

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_level** | **f64** | level where this segment starts, based on OpenStreetMap data | 
**to_level** | **f64** | level where this segment starts, based on OpenStreetMap data | 
**osm_way** | Option<**i32**> | OpenStreetMap way index | [optional]
**polyline** | [**models::EncodedPolyline**](EncodedPolyline.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



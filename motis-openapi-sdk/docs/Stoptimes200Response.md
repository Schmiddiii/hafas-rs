# Stoptimes200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stop_times** | [**Vec<models::StopTime>**](StopTime.md) | list of stop times | 
**previous_page_cursor** | **String** | Use the cursor to get the previous page of results. Insert the cursor into the request and post it to get the previous page. The previous page is a set of stop times BEFORE the first stop time in the result.  | 
**next_page_cursor** | **String** | Use the cursor to get the next page of results. Insert the cursor into the request and post it to get the next page. The next page is a set of stop times AFTER the last stop time in this result.  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



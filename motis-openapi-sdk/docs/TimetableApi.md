# \TimetableApi

All URIs are relative to *https://europe.motis-project.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stoptimes**](TimetableApi.md#stoptimes) | **GET** /api/v1/stoptimes | Get the next N departures or arrivals of a stop sorted by time
[**trip**](TimetableApi.md#trip) | **GET** /api/v1/trip | Get a trip as itinerary



## stoptimes

> models::Stoptimes200Response stoptimes(stop_id, n, time, arrive_by, direction, mode, radius, page_cursor)
Get the next N departures or arrivals of a stop sorted by time

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**stop_id** | **String** | stop id of the stop to retrieve departures/arrivals for | [required] |
**n** | **i32** | the number of events | [required] |
**time** | Option<**String**> | Optional. Defaults to the current time.  |  |
**arrive_by** | Option<**bool**> | Optional. Default is `false`.    - `arriveBy=true`: the parameters `date` and `time` refer to the arrival time   - `arriveBy=false`: the parameters `date` and `time` refer to the departure time  |  |[default to false]
**direction** | Option<**String**> | This parameter will be ignored in case `pageCursor` is set.  Optional. Default is   - `LATER` for `arriveBy=false`   - `EARLIER` for `arriveBy=true`  The response will contain the next `n` arrivals / departures in case `EARLIER` is selected and the previous `n` arrivals / departures if `LATER` is selected.  |  |
**mode** | Option<[**Vec<models::Mode>**](models::Mode.md)> | Optional. Default is all transit modes.  Only return arrivals/departures of the given modes.  |  |[default to ["TRANSIT"]]
**radius** | Option<**i32**> | Optional. Radius in meters.  Default is that only stop times of the parent of the stop itself and all stops with the same name (+ their child stops) are returned.  If set, all stops at parent stations and their child stops in the specified radius are returned.  |  |
**page_cursor** | Option<**String**> | Use the cursor to go to the next \"page\" of stop times. Copy the cursor from the last response and keep the original request as is. This will enable you to search for stop times in the next or previous time-window.  |  |

### Return type

[**models::Stoptimes200Response**](stoptimes_200_response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## trip

> models::Itinerary trip(trip_id)
Get a trip as itinerary

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**trip_id** | **String** | trip identifier (e.g. from an itinerary leg or stop event) | [required] |

### Return type

[**models::Itinerary**](Itinerary.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# Itinerary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **i32** | journey duration in seconds | 
**start_time** | **String** | journey departure time | 
**end_time** | **String** | journey arrival time | 
**transfers** | **i32** | The number of transfers this trip has. | 
**legs** | [**Vec<models::Leg>**](Leg.md) | Journey legs | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



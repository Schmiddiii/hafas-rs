# Place

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name of the transit stop / PoI / address | 
**stop_id** | Option<**String**> | The ID of the stop. This is often something that users don't care about. | [optional]
**lat** | **f64** | latitude | 
**lon** | **f64** | longitude | 
**level** | **f64** | level according to OpenStreetMap | 
**arrival** | Option<**String**> | arrival time | [optional]
**departure** | Option<**String**> | departure time | [optional]
**scheduled_arrival** | Option<**String**> | scheduled arrival time | [optional]
**scheduled_departure** | Option<**String**> | scheduled departure time | [optional]
**scheduled_track** | Option<**String**> | scheduled track from the static schedule timetable dataset | [optional]
**track** | Option<**String**> | The current track/platform information, updated with real-time updates if available.  Can be missing if neither real-time updates nor the schedule timetable contains track information.  | [optional]
**vertex_type** | Option<[**models::VertexType**](VertexType.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



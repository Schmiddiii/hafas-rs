# \RoutingApi

All URIs are relative to *https://europe.motis-project.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**plan**](RoutingApi.md#plan) | **GET** /api/v1/plan | Computes optimal connections from one place to another.



## plan

> models::Plan200Response plan(from_place, to_place, detailed_transfers, via, via_minimum_stay, time, max_transfers, max_travel_time, min_transfer_time, additional_transfer_time, transfer_time_factor, max_matching_distance, pedestrian_profile, use_routed_transfers, transit_modes, direct_modes, pre_transit_modes, post_transit_modes, direct_rental_form_factors, pre_transit_rental_form_factors, post_transit_rental_form_factors, direct_rental_propulsion_types, pre_transit_rental_propulsion_types, post_transit_rental_propulsion_types, direct_rental_providers, pre_transit_rental_providers, post_transit_rental_providers, num_itineraries, page_cursor, timetable_view, arrive_by, search_window, require_bike_transport, max_pre_transit_time, max_post_transit_time, max_direct_time, timeout)
Computes optimal connections from one place to another.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**from_place** | **String** | \\`latitude,longitude,level\\` tuple in degrees OR stop id | [required] |
**to_place** | **String** | \\`latitude,longitude,level\\` tuple in degrees OR stop id | [required] |
**detailed_transfers** | **bool** | - true: Compute transfer polylines and step instructions. - false: Only return basic information (start time, end time, duration) for transfers.  | [required] |[default to true]
**via** | Option<[**Vec<String>**](String.md)> | List of via stops to visit (only stop IDs, no coordinates allowed for now). Also see the optional parameter `viaMinimumStay` to set a set a minimum stay duration for each via stop.  |  |
**via_minimum_stay** | Option<[**Vec<i32>**](i32.md)> | Optional. If not set, the default is `0,0` - no stay required.  For each `via` stop a minimum stay duration in minutes.  The value `0` signals that it's allowed to stay in the same trip. This enables via stays without counting a transfer and can lead  to better connections with less transfers. Transfer connections can still be found with `viaMinimumStay=0`.  |  |[default to [0,0]]
**time** | Option<**String**> | Optional. Defaults to the current time.  Departure time ($arriveBy=false) / arrival date ($arriveBy=true),  |  |
**max_transfers** | Option<**i32**> | The maximum number of allowed transfers. If not provided, the routing uses the server-side default value which is hardcoded and very high to cover all use cases.  *Warning*: Use with care. Setting this too low can lead to optimal (e.g. the fastest) journeys not being found. If this value is too low to reach the destination at all, it can lead to slow routing performance.  |  |
**max_travel_time** | Option<**i32**> | The maximum travel time in minutes. If not provided, the routing to uses the value hardcoded in the server which is usually quite high.  *Warning*: Use with care. Setting this too low can lead to optimal (e.g. the least transfers) journeys not being found. If this value is too low to reach the destination at all, it can lead to slow routing performance.  |  |
**min_transfer_time** | Option<**i32**> | Optional. Default is 0 minutes.  Minimum transfer time for each transfer in minutes.  |  |[default to 0]
**additional_transfer_time** | Option<**i32**> | Optional. Default is 0 minutes.  Additional transfer time reserved for each transfer in minutes.  |  |[default to 0]
**transfer_time_factor** | Option<**f64**> | Optional. Default is 1.0  Factor to multiply minimum required transfer times with. Values smaller than 1.0 are not supported.  |  |[default to 1.0]
**max_matching_distance** | Option<**f64**> | Optional. Default is 25 meters.  Maximum matching distance in meters to match geo coordinates to the street network.  |  |[default to 25]
**pedestrian_profile** | Option<[**PedestrianProfile**](.md)> | Optional. Default is `FOOT`.  Accessibility profile to use for pedestrian routing in transfers between transit connections, on the first mile, and last mile.  |  |[default to FOOT]
**use_routed_transfers** | Option<**bool**> | Optional. Default is `false`.  Whether to use transfers routed on OpenStreetMap data.  |  |[default to false]
**transit_modes** | Option<[**Vec<models::Mode>**](models::Mode.md)> | Optional. Default is `TRANSIT` which allows all transit modes (no restriction). Allowed modes for the transit part. If empty, no transit connections will be computed. For example, this can be used to allow only `METRO,SUBWAY,TRAM`.  |  |[default to [TRANSIT]]
**direct_modes** | Option<[**Vec<models::Mode>**](models::Mode.md)> | Optional. Default is `WALK` which will compute walking routes as direct connections.  Modes used for direction connections from start to destination without using transit. Results will be returned on the `direct` key.  Note: Direct connections will only be returned on the first call. For paging calls, they can be omitted.  Note: Transit connections that are slower than the fastest direct connection will not show up. This is being used as a cut-off during transit routing to speed up the search. To prevent this, it's possible to send two separate requests (one with only `transitModes` and one with only `directModes`).  Only non-transit modes such as `WALK`, `BIKE`, `CAR`, `BIKE_SHARING`, etc. can be used.  |  |[default to ["WALK"]]
**pre_transit_modes** | Option<[**Vec<models::Mode>**](models::Mode.md)> | Optional. Default is `WALK`. Only applies if the `from` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directModes`).  A list of modes that are allowed to be used from the `from` coordinate to the first transit stop. Example: `WALK,BIKE_SHARING`.  |  |[default to [WALK]]
**post_transit_modes** | Option<[**Vec<models::Mode>**](models::Mode.md)> | Optional. Default is `WALK`. Only applies if the `to` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directModes`).  A list of modes that are allowed to be used from the last transit stop to the `to` coordinate. Example: `WALK,BIKE_SHARING`.  |  |[default to [WALK]]
**direct_rental_form_factors** | Option<[**Vec<models::RentalFormFactor>**](models::RentalFormFactor.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies to direct connections.  A list of vehicle type form factors that are allowed to be used for direct connections. If empty (the default), all form factors are allowed. Example: `BICYCLE,SCOOTER_STANDING`.  |  |
**pre_transit_rental_form_factors** | Option<[**Vec<models::RentalFormFactor>**](models::RentalFormFactor.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies if the `from` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directRentalFormFactors`).  A list of vehicle type form factors that are allowed to be used from the `from` coordinate to the first transit stop. If empty (the default), all form factors are allowed. Example: `BICYCLE,SCOOTER_STANDING`.  |  |
**post_transit_rental_form_factors** | Option<[**Vec<models::RentalFormFactor>**](models::RentalFormFactor.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies if the `to` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directRentalFormFactors`).  A list of vehicle type form factors that are allowed to be used from the last transit stop to the `to` coordinate. If empty (the default), all form factors are allowed. Example: `BICYCLE,SCOOTER_STANDING`.  |  |
**direct_rental_propulsion_types** | Option<[**Vec<models::RentalPropulsionType>**](models::RentalPropulsionType.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies to direct connections.  A list of vehicle type form factors that are allowed to be used for direct connections. If empty (the default), all propulsion types are allowed. Example: `HUMAN,ELECTRIC,ELECTRIC_ASSIST`.  |  |
**pre_transit_rental_propulsion_types** | Option<[**Vec<models::RentalPropulsionType>**](models::RentalPropulsionType.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies if the `from` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directRentalPropulsionTypes`).  A list of vehicle propulsion types that are allowed to be used from the `from` coordinate to the first transit stop. If empty (the default), all propulsion types are allowed. Example: `HUMAN,ELECTRIC,ELECTRIC_ASSIST`.  |  |
**post_transit_rental_propulsion_types** | Option<[**Vec<models::RentalPropulsionType>**](models::RentalPropulsionType.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies if the `to` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directRentalPropulsionTypes`).  A list of vehicle propulsion types that are allowed to be used from the last transit stop to the `to` coordinate. If empty (the default), all propulsion types are allowed. Example: `HUMAN,ELECTRIC,ELECTRIC_ASSIST`.  |  |
**direct_rental_providers** | Option<[**Vec<String>**](String.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies to direct connections.  A list of rental providers that are allowed to be used for direct connections. If empty (the default), all providers are allowed.  |  |
**pre_transit_rental_providers** | Option<[**Vec<String>**](String.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies if the `from` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directRentalProviders`).  A list of rental providers that are allowed to be used from the `from` coordinate to the first transit stop. If empty (the default), all providers are allowed.  |  |
**post_transit_rental_providers** | Option<[**Vec<String>**](String.md)> | Experimental. Expect unannounced breaking changes (without version bumps).  Optional. Only applies if the `to` place is a coordinate (not a transit stop). Does not apply to direct connections (see `directRentalProviders`).  A list of rental providers that are allowed to be used from the last transit stop to the `to` coordinate. If empty (the default), all providers are allowed.  |  |
**num_itineraries** | Option<**i32**> | The minimum number of itineraries to compute. This is only relevant if `timetableView=true`. The default value is 5.  |  |[default to 5]
**page_cursor** | Option<**String**> | Use the cursor to go to the next \"page\" of itineraries. Copy the cursor from the last response and keep the original request as is. This will enable you to search for itineraries in the next or previous time-window.  |  |
**timetable_view** | Option<**bool**> | Optional. Default is `true`.  Search for the best trip options within a time window. If true two itineraries are considered optimal if one is better on arrival time (earliest wins) and the other is better on departure time (latest wins). In combination with arriveBy this parameter cover the following use cases:  `timetable=false` = waiting for the first transit departure/arrival is considered travel time:   - `arriveBy=true`: event (e.g. a meeting) starts at 10:00 am,     compute the best journeys that arrive by that time (maximizes departure time)   - `arriveBy=false`: event (e.g. a meeting) ends at 11:00 am,     compute the best journeys that depart after that time  `timetable=true` = optimize \"later departure\" + \"earlier arrival\" and give all options over a time window:   - `arriveBy=true`: the time window around `date` and `time` refers to the arrival time window   - `arriveBy=false`: the time window around `date` and `time` refers to the departure time window  |  |[default to true]
**arrive_by** | Option<**bool**> | Optional. Default is `false`.    - `arriveBy=true`: the parameters `date` and `time` refer to the arrival time   - `arriveBy=false`: the parameters `date` and `time` refer to the departure time  |  |[default to false]
**search_window** | Option<**i32**> | Optional. Default is 2 hours which is `7200`.  The length of the search-window in seconds. Default value two hours.    - `arriveBy=true`: number of seconds between the earliest departure time and latest departure time   - `arriveBy=false`: number of seconds between the earliest arrival time and the latest arrival time  |  |[default to 7200]
**require_bike_transport** | Option<**bool**> | Optional. Default is `false`.  If set to `true`, all used transit trips are required to allow bike carriage.  |  |[default to false]
**max_pre_transit_time** | Option<**i32**> | Optional. Default is 15min which is `900`. Maximum time in seconds for the first street leg.  |  |[default to 900]
**max_post_transit_time** | Option<**i32**> | Optional. Default is 15min which is `900`. Maximum time in seconds for the last street leg.  |  |[default to 900]
**max_direct_time** | Option<**i32**> | Optional. Default is 30min which is `1800`. Maximum time in seconds for direct connections.  |  |[default to 1800]
**timeout** | Option<**i32**> | Optional. Query timeout in seconds. |  |

### Return type

[**models::Plan200Response**](plan_200_response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# Rental

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**system_id** | **String** | Vehicle share system ID | 
**system_name** | Option<**String**> | Vehicle share system name | [optional]
**url** | Option<**String**> | URL of the vehicle share system | [optional]
**station_name** | Option<**String**> | Name of the station | [optional]
**rental_uri_android** | Option<**String**> | Rental URI for Android (deep link to the specific station or vehicle) | [optional]
**rental_uri_ios** | Option<**String**> | Rental URI for iOS (deep link to the specific station or vehicle) | [optional]
**rental_uri_web** | Option<**String**> | Rental URI for web (deep link to the specific station or vehicle) | [optional]
**form_factor** | Option<[**models::RentalFormFactor**](RentalFormFactor.md)> |  | [optional]
**propulsion_type** | Option<[**models::RentalPropulsionType**](RentalPropulsionType.md)> |  | [optional]
**return_constraint** | Option<[**models::RentalReturnConstraint**](RentalReturnConstraint.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Match

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | **String** | location type | 
**tokens** | [**Vec<Vec<f64>>**](Vec.md) | list of non-overlapping tokens that were matched | 
**name** | **String** | name of the location (transit stop / PoI / address) | 
**id** | **String** | unique ID of the location | 
**lat** | **f64** | latitude | 
**lon** | **f64** | longitude | 
**level** | Option<**f64**> | level according to OpenStreetMap (at the moment only for public transport)  | [optional]
**street** | Option<**String**> | street name | [optional]
**house_number** | Option<**String**> | house number | [optional]
**zip** | Option<**String**> | zip code | [optional]
**areas** | [**Vec<models::Area>**](Area.md) | list of areas | 
**score** | **f64** | score according to the internal scoring system (the scoring algorithm might change in the future) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



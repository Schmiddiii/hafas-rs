curl -o openapi.yml https://raw.githubusercontent.com/motis-project/motis/refs/heads/master/openapi.yaml
openapi-generator-cli generate -g rust -i openapi.yml --package-name motis-openapi-sdk

# Bug in the generator, fixed in https://github.com/OpenAPITools/openapi-generator/pull/19946
sed -i 's/version = "1"/version = "1.0.0"/' Cargo.toml

# Disable clippy on generated code.
sed -i '1s/^/#![allow(clippy::all)]\n/' src/lib.rs

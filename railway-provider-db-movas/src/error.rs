use std::fmt::Display;

#[derive(Debug)]
pub enum Error {
    Json(serde_json::Error),
    RefreshJourneyNotFound,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match &self {
            Self::Json(e) => write!(f, "json error: {}", e),
            Self::RefreshJourneyNotFound => write!(f, "refresh journey not found"),
        }
    }
}

impl std::error::Error for Error {}

# Writing a Provider

This document lays out the steps required to implement a provider for Railway.
For a successful implementation, you will need basic Rust skills.
The provider you want to implement furthermore must have a public API, ideally including documentation.

For a simple example, take a look at `railway-provider-search-ch`.

## Set up Crate

The backend of Railway was built such that each API has its own crate.
Create a new crate with `cargo new --lib railway-provider-<name>`, replacing `name` with the name of the provider you are implementing in kebab-case.
Make sure your crate was added to the workspace in the root `Cargo.toml`.

## Setting up `Cargo.toml`

### Dependencies

You will need the following dependencies:

- `railway-core` (often aliased to `rcore`): Contains the core type definitions used by Railway and your implementation.
- `async-trait`: Allows implementing the asynchronous provider trait.
- `chrono` and `chrono-tz`: For working with dates and times.

You most likely will also need `serde` (and possibly `serde_json`) for working with your API and `url` for typed URLs.

For `dev-dependencies`, you may also want `tokio` with `macros`-feature, `env_logger` and `railway-core` with `hyper-requester`-feature.

### Features

You will need the following features:

```toml
[features]
rt-multi-thread = [ "rcore/rt-multi-thread" ]
polylines = [ "rcore/polylines" ]
```

`rt-multi-thread` allows for using a provider across multiple threads, and `polylines` allows adding polylines to the results.

### Other

- Set up a license for your provider. This project uses mostly "AGPL-3.0-or-later OR EUPL-1.2".

## Implement Provider

Implement the actual provider standalone.

### Struct

Set up your struct which will be used as the provider.
As per the naming convention, use `<Name>Client`, replacing `Name` with the name of the provider in PascalCase.
This provider should be generic over `R: Requester`, which also is one of the structs fields `requester`.

### Constructor

You need a way to construct your struct using a `RequesterBuilder`.
The API usually looks similar to this, but may have further arguments:

```rust
impl<R: Requester> <Name>Client<R> {
    pub fn new<RB: RequesterBuilder<Requester = R>>(requester: RB) -> Self {
        ...
    }
```

The `RequesterBuilder` allows adding custom certificates that are accepted, if required, and then build your `Requester`.

### Provider

Implement your provider with the following `impl`-block:

```rust
#[cfg_attr(feature = "rt-multi-thread", async_trait)]
#[cfg_attr(not(feature = "rt-multi-thread"), async_trait(?Send))]
impl<R: Requester> Provider<R> for <Name>Client<R> {
    type Error = ...;

    ...
}
```

The config attributes ensure correct usage of `async_trait`.
You should also have a custom error type, which could for example include `serde`-errors.

You then need to implement all required methods.
Particularly note that in the implementation to convert errors with the requester to errors you can return, use `.map_err(rcore::Error::Request)`

### Test

Set up tests for your provider in particular.
Those tests usually include one for searching locations and searching for a journey.
Feel free to copy code from other providers here, e.g. `check_search` or `check_journey`.

## Integrate into API

To integrate your new provider into `railway-api`, you need to do the following:

### Add to `Cargo.toml`

Add your provider to the `Cargo.toml` of `railway-api`.
This includes:

- Adding the package (usually aliased `r<name>`), without default features and optional.
- Add your package features to `rt-multi-thread` and `polylines` features.
- Add a feature `<name>-provider` which activates the dependency.
- Add your feature to `all-providers` (assuming it is in a working state).

### Add to `RailwayProvider`

Add a variant to the `RailwayProvider` enum, which includes a `cfg` which only activates if your feature is active, the `provider`-annotation with a `constructor` which shows how to construct your provider, the enum variant itself named after your provider and which holds your providers `Client`.

## Document in `README.md`

When you now run all your tests, the `readme_supported_up_to_date` test of `railway-api` should fail.
This indicates that the list of supported providers in the `README.md` is up-to-date.
Add your provider to this list, while naming its short name (usually abbreviation) and long name.

### (Optional) Add to CI

Your tests will already be run in CI, but will be "general tests", i.e. tests that are now allowed to fail.
As provider tests are allowed to fail sometimes, e.g. due to DNS issues, you should move the tests to "provider tests".
For that, exclude your crate in `test-general-latest` and include it in `test-provider-latest`.

## (Optional) Add to Railway

To add the provider to [Railway](https://gitlab.com/schmiddi-on-mobile/railway) itself, add it to the list returned by the `providers`-function in `src/backend/client.rs`.
This listing should include the identifier under which it is listed in `RailwayProviders`, the short and long name and whether it has an icon.
To construct an icon for your provider, please ask in the [Matrix channel](https://matrix.to/#/#railwayapp:matrix.org).

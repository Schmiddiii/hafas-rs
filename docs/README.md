# Developer Documentation

This folder contains documentation aimed at developers working on `railway-backend`.
It includes:

- [Architecture Description (including design decisions)](./architecture.md)
- [Writing a Provider](./writing-a-provider.md)
- [Information about the CI](./ci.md)
